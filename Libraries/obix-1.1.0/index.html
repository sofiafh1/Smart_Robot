<html>

<head>
<title>Java oBIX Toolkit</title>

<style type="text/css">
<!--
body
{
  background: #ffffff;
}

h1
{
  color: #000;
  background: #eee;
  border-bottom: 1px solid black;
  padding-left: 5px;
  font-size: 18pt;
}

h1.title
{
  color: #669;
  background: none;
  font-size: 24pt;
  border: none;
}

h2
{
  font-size: 16pt;
  padding-left: 0.5em;
  width: 40%;
}

h3, h4, h5, h6
{
  font-size: 14pt;
  padding-left: 0.5em;
}

p
{
  padding-left: 10px;
  padding-right: 10px;
}

pre
{
  font-family: monospace;
  padding-left: 4em;
  color: #008000;
}

ul
{
  padding-left: 2em;
}

ol
{
  padding-left: 2em;
}

li
{
  margin: 0.3em;
}

table
{
  padding-left: 3em;
}


-->
</style>

</head>

<body>

<!-- Title Block -->
<h1 class='title'>Java oBIX Toolkit</h1>

<!------------------------------------------------------------->
<h1 id="overview">Contents</h1>
<!------------------------------------------------------------->

<ul>
<li><b>index.html</b>: this file</li>
<li><b>obix.jar</b>: precompiled Java jar</li>
<li><b>src/</b>: source code files</li>
<li><b>doc/</b>: the <a href='doc/index.html'>JavaDocs</a></li>
</ul>

<!------------------------------------------------------------->
<h1 id="overview">Overview</h1>
<!------------------------------------------------------------->

<p>
The Java oBIX Toolkit is an open source, public domain
library to support oBIX developers.  From a broad perspective
oBIX provides a basic data model defined in terms of object
trees that can be shared as XML documents.  To learn more
about oBIX see <a href="http://www.obix.org/">www.obix.org</a>.
</p>

<p>
This toolkit serves two purposes.  Its first purpose is
to provide working code to help prove out the specification
and test how easy it is to implement.  Its second purpose in
life is as an open source library to help developers quickly
oBIX enable their own applications.
</p>

<p>
Specifically what this toolkit provides:
</p>

<ul>
<li><a href="#model">Object Model</a></li>
<li><a href="#io">IO API</a></li>
<li><a href="#session">Obix Session</a></li>
<li><a href="#compiler">Obix Compiler</a></li>
<li><a href="#spy">Obix Spy</a></li>
<li><a href="#asm">Java bytecode assembler</a></li>
<li><a href="#xml">XML Pull Parser</a></li>
<li><a href="#license">Open sourced as public domain</a></li>
</ul>

<p>
No additional libraries are needed to use this code - it is dependency free!
With the exception of the obix.ui Swing code, this toolkit is J2ME compliant
and should run in most embedded environments.  However, there is one method
being used in Abstime.java which is only available in J2SE 1.4+.  That
method is TimeZone.getDSTSavings(), and can potentially be replaced
by casting the TimeZone to a SimpleTimeZone.
</p>

<!------------------------------------------------------------->
<h1 id="model">Object Model</h1>
<!------------------------------------------------------------->

<p>
The <code>obix</code> package provides a set of classes for modeling
each of the oBIX built-in object types: Obj, Bool, Int, etc.  These
classes provide support for managing tree structure, contract definitions,
and encoding/decoding of primitive data types.
</p>

<!------------------------------------------------------------->
<h1 id="io">IO API</h1>
<!------------------------------------------------------------->

<p>
The <code>obix.io.ObixEncoder</code> class takes an <code>Obj</code>
tree and generates its XML document representation.  Likewise the
<code>obix.io.ObixDecoder</code> parses an XML document into memory
as a tree of <code>Obj</code> instances.  The <code>obix.io.BinObixEncoder</code>
and <code>obix.io.BinObixDecoder</code> are used for binary serialization.
</p>

<!------------------------------------------------------------->
<h1 id="session">Obix Session</h1>
<!------------------------------------------------------------->

<p>
The <code>obix.net.ObixSession</code> class provides a simple
REST-HTTP client side implementation of oBIX.  Currently it only
supports HTTP Basic authentication.  This code is largely a wrapper
for other libraries such as <code>obix</code> and <code>obix.io</code>.
The <code>ObixSession</code> API provides built-in support for client side
watches via the <code>SessionWatch</code> class.  The
<code>ObixSession</code> comes with a very simple command line
program you can use to read and dump oBIX objects from a URI.  To
run "java -cp obix.jar obix.net.ObixSession".
</p>

<!------------------------------------------------------------->
<h1 id="compiler">Obix Compiler</h1>
<!------------------------------------------------------------->

<p>
This toolkit contains the ability to dynamically map arbitrary
contract lists into strongly typed Java classes:
</p>

<ul>

<li><b>ContractRegistry</b>: maintains a map of contract URIs to
Java interface classes.</li>

<li><b>ObixAssembler</b>: dynamically generates a class which
extends from one of the Obj types (Obj, Int, List, etc) and
implements one or more interaces mapped by ContractRegistry.</li>

<li><b>Obixc</b>:  a tool which inputs an oBIX XML document and
outputs Java source code - an interface for each contract.
You can run this tool from the command line:
"java -cp obix.jar obix.tools.Obixc"</li>

</ul>

<p>
You can pretty much throw any combination of contracts at it
that have a predefined Java interface and you are guaranteed to
get a strongly typed class:
</p>

<pre>
  Obj obj = ObixDecoder.fromString("&lt;enum is='obix:Point obix:History'/&gt;");
  verify(obj instanceof obix.Enum);
  verify(obj instanceof obix.contracts.Point);
  verify(obj instanceof obix.contracts.History);
</pre>

<p>
The interfaces themselves must extend from IObj and contain a
getter method for each child object in the contract.  If the
child contains non-standard defaults such as its own contract
list or facets, then you should also declare a contract field.
The following is a simple example of an oBIX contract and
what obixc will generate for its Java interface:
</p>

<pre>
  &lt;obj href='obix:Dummy'&gt;
     &lt;str name='foo'&gt;
     &lt;int name='bar' min='0'/&gt;
  &lt;/obj&gt;

  public interface Dummy extends IObj
  {
    public Str foo();

    public Int bar();
    public static final String barContract = "&lt;int name='bar' min='0'/&gt;";
  }
</pre>

<p>
The <code><a href='src/stdlib.obix'>stdlib.obix</a></code> file
which contains the contracts from the specification itself have
been precompiled into the <code>obix.contracts</code> package.
</p>

<!------------------------------------------------------------->
<h1 id="spy">Obix Spy</h1>
<!------------------------------------------------------------->

<p>
The <code>obix.ui.Shell</code> class is a simple Swing application
that provides a GUI for "browsing" oBIX servers.  It works just like
a browser navigating via URIs to oBIX object documents.  This tool
demonstrates how to use the rest of the APIs such as <code>ObixSession</code>.
It also is a handy diagnostics and testing tool.  You can run this
application by double clicking obix.jar or you can run from the
command line: "java -jar obix.jar" or "java -cp obix.jar obix.ui.Shell".
It should run with Java 1.4 or 1.5 (maybe 1.3 too).
</p>

<!------------------------------------------------------------->
<h1 id="asm">Assembler</h1>
<!------------------------------------------------------------->

<p>
The <code>obix.asm</code> provides a lean, mean Java
bytecode assembler library used to auto-generate typed Java classes
for contract lists.  This library may be easily broken out as a
standalone module.
</p>

<!------------------------------------------------------------->
<h1 id="xml">XML Pull Parser</h1>
<!------------------------------------------------------------->

<p>
The <code>obix.xml</code> package is an simple light weight (21kb) open source
implementation of an XML pull/DOM style parser.  It is available stand alone
at <a href="http://sourceforge.net/projects/uxparser">sourceforge.net/projects/uxparser</a>.
This code is J2ME compliant and allows the Java oBIX Toolkit
to remain unencumbered by any unwieldy dependencies.
</p>

<!------------------------------------------------------------->
<h1 id="license">License</h1>
<!------------------------------------------------------------->

<p>
This code is open source and public domain - there are no legal
restrictions placed upon it.  You may do whatever the heck you
want to with it.
</p>

<!------------------------------------------------------------->
<h1 id="changeLog">Todo</h1>
<!------------------------------------------------------------->

<p>This is hardly a complete client side implementation,
although it probably has just about everything needed for
to yourself bootstraped with oBIX.  Still the following things
make up a nice wish list:
</p>

<ul>
<li>Facets support in the UI such as populating enum drop downs
and doing min/max checking</li>
<li>HistoryView to query/watch histories</li>
<li>Improved XML namespace and prefix support in contracts;</li>
<li>Credentials management for logging in (right now you get one
chance to enter a username/password)</li>
<li>Bookmarks are at least a persistent history cache</li>
<li>Abstime editor</li>
<li>Alternate HTTP authentication support (digest, etc)</li>
<li>HTTPS support</li>
<li>SOAP support</li>
</ul>

<!------------------------------------------------------------->
<h1 id="changeLog">Change Log</h1>
<!------------------------------------------------------------->

<p><b>Version 1.1.0 (2 Nov 09)</b>
<ul>
<li>Time and Date objects</li>
<li>Support for tz facet</li>
<li>Binary encoding and decoding</li>
</ul>
</p>

<p><b>Version 1.0.2 (03 Dec 08)</b>
<ul>
<li>Remove unnecessary imports and unused variables</li>
</ul>
</p>

<p><b>Version 1.0.1 (11 Nov 08)</b>
<ul>
<li>Improve management of poll period in poll loop in SessionWatch</li>
<li>Fixed several typos in comments</li>
<li>Provide API to iterate hrefs in SessionWatch</li>
<li>Handle null authority in Uri.parent()</li>
<li>Improve use of ContractRegistry for core obix contracts</li>
</ul>
</p>

<p><b>Version 1.0.0 (26 Oct 06)</b>
<ul>
<li>Uri resolve support for fragment identifiers (Ref.href, is, of, in, out, units, range)</li>
<li>Fixed batch writing and invocation</li>
<li>Expired sessions are now cleaned up.  New closed callback in WatchListener.</li>
<li>Removed Abstime dependency on JDK 5.0.</li>
</ul>
</p>

<p><b>Version 0.12.0 (1 Jun 06)</b>
<ul>
<li>Support for latest 0.12 object model (feeds)</li>
<li>ObixSession support for batch requests</li>
<li>Obixc generates only interfaces with contract fields</li>
<li>Dynamic class generation for contract mapping</li>
<li>obix.ui.View support for watches (with automatic cleanup)</li>
<li>AlarmConsoleView on obix:AlarmSubject</li>
</ul>
</p>

<p><b>Version 0.11.0 (25 Apr 06)</b>
<ul>
<li>Full API support for all facets</li>
<li>ObjSheet improvements (icons, writable)</li>
<li>Operation invoke with arguments</li>
<li>Tree refresh</li>
<li>0.10 to 0.11 updates (min/max for Str, List)</li>
</ul>
</p>

<p><b>Version 0.10.0 (6 Apr 06)</b>
<ul>
<li>Support for latest 0.10 object model</li>
<li>Basic handling of covariance in ObixEncoder</li>
<li>Support for Watches in ObixSession</li>
<li>Navigation tree in swing tool</li>
<li>Icon support for swing tool</li>
<li>Pluggable view tabs for swing tool</li>
<li>Support for managing watches in swing tool</li>
</ul>
</p>

<p><b>Version 0.8.3 (6 Oct 05)</b>
<ul>
<li>Initial release to SourceForge</li>
</ul>
</p>


</body>
</html>

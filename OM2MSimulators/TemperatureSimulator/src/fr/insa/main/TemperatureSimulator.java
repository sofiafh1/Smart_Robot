package fr.insa.main;

import java.io.IOException;

import fr.v2.om2m.entities.ApplicationEntity;
import fr.v2.om2m.entities.Container;
import fr.v2.om2m.entities.ContentInstance;
import fr.v2.om2m.utils.ApplicationEntityConfig;
import fr.v2.om2m.utils.ContainerConfig;
import fr.v2.om2m.utils.ContentInstanceConfig;
import fr.v2.om2m.utils.HeaderSet;
import obix.Obj;
import obix.Str;

public class TemperatureSimulator {

	private static ApplicationEntity tempSimuApp;
	private static Container dataContainer;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int minVal = 10;
		int maxVal = 40;
		
		tempSimuApp = createApplication();
		
		// create containers
		Container descriptorContainer = createDescriptorContainer(tempSimuApp);

		addDescriptorDescription(descriptorContainer);

		dataContainer = createDataContainer(tempSimuApp);
		
		ContentInstanceConfig dataConfig = new ContentInstanceConfig();
		dataConfig.parentContainer = dataContainer;
		dataConfig.headers = HeaderSet.generateDefaultContentInstanceHeaderSet();
		dataConfig.contentInfo = "message";
		
		// create dummy data
		for (int i = 0; i < 100; i++) {
			dataConfig.content = new Obj();
			dataConfig.content.add(new Str("appId", "TemperatureSensorSimulator"));
			dataConfig.content.add(new Str("category", "temperature"));
			
			// generate random data
			float randomData = minVal + (int)(Math.random() * ((maxVal - minVal) + 1));
			
			dataConfig.content.add(new Str("data", Float.toString(randomData)));
			dataConfig.content.add(new Str("unit", "celcius"));
			ContentInstance.createContentInstanceOnRemote(dataConfig);
		}
		
		// loop waiting for user entry to exit
		promptEnterKey();
		
		// delete application
		tempSimuApp.removeApplicationFromRemote();
		System.out.println("remote application deleted");
	}
	
	private static ApplicationEntity createApplication() {
		ApplicationEntityConfig tempSensorConfig = new ApplicationEntityConfig();
		tempSensorConfig.resourceName = "TemperatureSensorSimulator";
		tempSensorConfig.rootUrl = "http://localhost:8080/~/mn-cse";
		
		tempSensorConfig.headers = HeaderSet.generateDefaultApplicationEntityHeaderSet();

		tempSensorConfig.labels.setLabel("Type", "sensor_simulator");
		tempSensorConfig.labels.setLabel("Location", "home");
		tempSensorConfig.labels.setLabel("appId", "TemperatureSensorSimulator");
		
		return ApplicationEntity.createApplicationOnRemote(tempSensorConfig);
	}

	private static Container createDescriptorContainer(ApplicationEntity parentAe) {
		ContainerConfig descriptorConfig = new ContainerConfig();
		descriptorConfig.resourceName = "DESCRIPTOR";
		descriptorConfig.headers = HeaderSet.generateDefaultContainerHeaderSet();
		descriptorConfig.parentApplication = parentAe;
		Container descriptorContainer = Container.createContainerOnRemote(descriptorConfig);
		return descriptorContainer;
	}
	
	private static void addDescriptorDescription(Container descriptor) {
		ContentInstanceConfig descriptionConfig = new ContentInstanceConfig();
		
		descriptionConfig.parentContainer = descriptor;
		descriptionConfig.contentInfo = "message";
		descriptionConfig.headers = HeaderSet.generateDefaultContentInstanceHeaderSet();
		
		descriptionConfig.content.add(new Str("type", "Temperature_Sensor_Simulator"));
		descriptionConfig.content.add(new Str("location", "Home"));
		descriptionConfig.content.add(new Str("appId", "TemperatureSensorSimulator"));
		
		ContentInstance.createContentInstanceOnRemote(descriptionConfig);
	}
	
	private static Container createDataContainer(ApplicationEntity parentAe) {
		ContainerConfig dataConfig = new ContainerConfig();
		dataConfig.resourceName = "DATA";
		dataConfig.headers = HeaderSet.generateDefaultContainerHeaderSet();
		dataConfig.parentApplication = parentAe;
		Container descriptorContainer = Container.createContainerOnRemote(dataConfig);
		return descriptorContainer;
	}

	public static void promptEnterKey(){
	    System.out.println("Press \"ENTER\" to kill simulator...");
	    try {
	        System.in.read();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
}

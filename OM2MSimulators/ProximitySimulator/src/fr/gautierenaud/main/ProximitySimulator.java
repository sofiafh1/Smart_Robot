package fr.gautierenaud.main;

import com.sun.org.apache.xpath.internal.operations.Bool;

import fr.v2.om2m.entities.ApplicationEntity;
import fr.v2.om2m.entities.Container;
import fr.v2.om2m.entities.ContentInstance;
import fr.v2.om2m.utils.ApplicationEntityConfig;
import fr.v2.om2m.utils.ContainerConfig;
import fr.v2.om2m.utils.ContentInstanceConfig;
import fr.v2.om2m.utils.HeaderSet;
import obix.Str;

public class ProximitySimulator {
	
	private ApplicationEntity proximitySensor;
	private Container proximityDescriptor;
	private Container proximityData;
	
	private String appId;
	private String sensorLocation;
	private boolean isPersonIn;
	
	public ProximitySimulator(String appId, String location) {
		this.appId = appId;
		this.sensorLocation = location;
		
		this.isPersonIn = false;
		
		proximitySensor = createProximityApplication();
		
		proximityDescriptor = createProximityDescriptor();
		addDescription();
		
		proximityData = createProximityDataContainer();
		
	}
	
	private ApplicationEntity createProximityApplication() {
		ApplicationEntityConfig proximityAeConfig = new ApplicationEntityConfig();
		
		proximityAeConfig.headers = HeaderSet.generateDefaultApplicationEntityHeaderSet();
		proximityAeConfig.apiType = "api-simulator";
		proximityAeConfig.credentials = "admin:admin";
		proximityAeConfig.resourceName = this.appId;
		proximityAeConfig.labels.setLabel("Type", "sensor_simulator");
		proximityAeConfig.labels.setLabel("Location", this.sensorLocation);
		proximityAeConfig.labels.setLabel("appId", this.appId);
		
		return ApplicationEntity.createApplicationOnRemote(proximityAeConfig);
	}
	
	private Container createProximityDescriptor() {
		ContainerConfig descriptorConfig = new ContainerConfig();
		descriptorConfig.resourceName = "DESCRIPTOR";
		descriptorConfig.headers = HeaderSet.generateDefaultContainerHeaderSet();
		descriptorConfig.parentApplication = this.proximitySensor;
		Container descriptorContainer = Container.createContainerOnRemote(descriptorConfig);
		return descriptorContainer;
	}
	
	private void addDescription() {
		ContentInstanceConfig descriptionConfig = new ContentInstanceConfig();
		
		descriptionConfig.parentContainer = this.proximityDescriptor;
		descriptionConfig.contentInfo = "message";
		descriptionConfig.headers = HeaderSet.generateDefaultContentInstanceHeaderSet();
		
		descriptionConfig.content.add(new Str("type", "Proximity_Sensor_Simulator"));
		descriptionConfig.content.add(new Str("location", this.sensorLocation));
		descriptionConfig.content.add(new Str("appId", this.appId));
		
		ContentInstance.createContentInstanceOnRemote(descriptionConfig);
	}
	
	private Container createProximityDataContainer() {
		ContainerConfig dataConfig = new ContainerConfig();
		dataConfig.resourceName = "DATA";
		dataConfig.headers = HeaderSet.generateDefaultContainerHeaderSet();
		dataConfig.parentApplication = this.proximitySensor;
		Container dataContainer = Container.createContainerOnRemote(dataConfig);
		return dataContainer;
	}
	
	public void togglePresence() {
		this.isPersonIn = !this.isPersonIn;
		addNewProximityData();
	}
	
	private void addNewProximityData() {
		ContentInstanceConfig dataConfig = new ContentInstanceConfig();
		
		dataConfig.parentContainer = this.proximityData;
		dataConfig.contentInfo = "message";
		dataConfig.headers = HeaderSet.generateDefaultContentInstanceHeaderSet();
		
		dataConfig.content.add(new Str("appId", "TemperatureSensorSimulator"));
		dataConfig.content.add(new Str("category", "temperature"));
		dataConfig.content.add(new Str("data", Boolean.toString(this.isPersonIn)));
		dataConfig.content.add(new Str("unit", "none"));
		
		
		ContentInstance.createContentInstanceOnRemote(dataConfig);
	}
	
	public void removeRemoteApplication() {
		this.proximitySensor.removeApplicationFromRemote();
	}
}

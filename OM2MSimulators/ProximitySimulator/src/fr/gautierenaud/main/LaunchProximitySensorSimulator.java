package fr.gautierenaud.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LaunchProximitySensorSimulator {
	
	private static ProximitySimulator proxSimu;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		proxSimu = new ProximitySimulator("proxSimu1", "bathroom");
		while(promptEnterKey()){}
		
		proxSimu.removeRemoteApplication();
	}

	public static boolean promptEnterKey(){
	    System.out.println("Type \"exit\" to delete the remote application and exit the program, anything else to toggle presence");
	    try {
	    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    	String s = br.readLine();
	    	
	        if (s.compareTo("exit") == 0) {
	        	return false;
	        } else {
	    		proxSimu.togglePresence();
	        	return true;
	        }
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return false;
	}
}

This is a student project for making a robot aware of it's environment using surrounding smartdevices (heartbeat, heat, light, ...), and help elderly people using these information.
The basic usecase we will see here is the robot detecting whenever the heart of the person is failing, getting the adaquate medication and bring it to the person.
We are working with the LAAS, using OM2M (http://www.eclipse.org/om2m/) to interact with the sensors.

<b>Configuration</b>
Raspberry:
	login:	pi
	pwd:	raspberry
	ipv4:	169.254.55.92

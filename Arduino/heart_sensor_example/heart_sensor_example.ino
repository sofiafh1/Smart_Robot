/*
 * Measures the time interval between each pulse
 * Uses a moving average to calculate heart rate
 * 
 */
#include <MsTimer2.h>

#define BLUETOOTH_SPEED 9600
#define MAX_PULSE_TIME_MS 2000
#define NB_SAMPLE_HEART_PULSE 10
#define MULT_MIN_TO_SEC 60
#define DATA_RATE_MS 5000

unsigned char counter;
unsigned long temp[NB_SAMPLE_HEART_PULSE];
unsigned long time_pulse;
unsigned long time_previous_pulse[2];
bool data_valid=true;

float heart_rate;

void measure_pulse_time();

void send_heart_rate_via_bluetooth();

void setup()
{
  //reset_pulse_measurements();
  
  //Initialise communication with the bluetooth module
  Serial.begin(BLUETOOTH_SPEED);
  //Execute function interrupt on rising edges on pin D0
  attachInterrupt(0, measure_pulse_time, RISING);//set interrupt 0,digital port 2
  //attach send_heart_rate_via_bluetooth to an interrupt with a time period of DATA_RATE_MS
  MsTimer2::set(DATA_RATE_MS, send_heart_rate_via_bluetooth); 
  MsTimer2::start(); 
}
void loop(){}

/** @brief calculates the heart rate and sends the data via the bluetooth communication
 *
 */
void send_heart_rate_via_bluetooth(){
  heart_rate=calculate_heart_rate(temp);
  //Send data via bluetooth connection
  Serial.println(heart_rate);
}

/** @brief      calculates the heart rate in beats per minute 
 *  @param[in]  unsigned long time_interval[NB_SAMPLE_HEART_PULSE], contains the time 
 *              for NB_SAMPLE_HEART_PULSE pulse samples
 *  return float heart_rate_bpm
 */
float calculate_heart_rate(unsigned long time_interval[]){
 float heart_rate_bpm;
 if(data_valid)
    {
      unsigned long total_time = 0;
      for (unsigned char i = 0; i < NB_SAMPLE_HEART_PULSE; i++){
        total_time += time_interval[i];
      }
      heart_rate_bpm=(float) MULT_MIN_TO_SEC*NB_SAMPLE_HEART_PULSE/((total_time/1000.0));//60*20*1000/20_total_time 
    }
   data_valid=1;//sign bit
   return heart_rate_bpm;
}
/*Function: Interrupt service routine.Get the sigal from the external interrupt*/
void measure_pulse_time()
{
    // Get time
    time_previous_pulse[0]=millis();
    unsigned long new_time_interval=millis()-time_previous_pulse[1];
    if (new_time_interval<MAX_PULSE_TIME_MS){
      data_valid=true;
      for (uint8_t i = NB_SAMPLE_HEART_PULSE-1; i>0; i--){
        switch(i){
            break;
          default:
            temp[i]=temp[i-1];
        }
      }
      temp[0]=new_time_interval;
    }
    else{
      data_valid=false;
    }
    time_previous_pulse[1]=time_previous_pulse[0];
}
/*Function: Initialization for the array(temp)*/
void reset_pulse_measurements(){
  for(unsigned char i=0;i < NB_SAMPLE_HEART_PULSE;i ++)
  {
    temp[i]=millis();
  }
  time_previous_pulse[1] = millis();
}

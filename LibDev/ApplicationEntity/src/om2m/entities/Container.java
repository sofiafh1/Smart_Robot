package om2m.entities;

import java.util.ArrayList;
import om2m.communication.Header;
import om2m.communication.HttpClientImpl;
import om2m.communication.Response;

public class Container {

	private String name;
	private Application parentApplication = null;
	
	public static Container createContainer(String nameToSet) {
		return new Container(nameToSet);
	}
	
	private Container(String nameToSet) {
		this.name = nameToSet;
	}

	public void addContentInstance(ContentInstance contentInstanceToAdd) {}
	
	public void createRemoteContainer(){		
		String remoteUrl = generateRemoteUrl();
		String containerXmlNotation = generateCreateContainerXml();
		ArrayList<Header> createContainerHeaders = generateCreateContainerHeaders();
		
		sendCreateRemoteContainerRequest(remoteUrl, containerXmlNotation, createContainerHeaders);
	}
	
	public String generateRemoteUrl() {
		if (this.parentApplication != null) {
			// String remoteUrl = this.parentApplication.getOm2mRootUrl();
			String remoteUrl = "toto"; //this.parentApplication.getOm2mRootUrl();
			remoteUrl += "/mn-name/" + this.parentApplication.getAppId();
			
			return remoteUrl;
		} else {
			System.err.println("Parent Application not setted, the container is unable to get the target URL");
			return null;
		}
	}
	
		private String generateCreateContainerXml() {
			String containerRepresentation = "<om2m:cnt xmlns:om2m=\"http://www.onem2m.org/xml/protocols\"></om2m:cnt>";
			return containerRepresentation;
	}
	
	private ArrayList<Header> generateCreateContainerHeaders() {
		ArrayList<Header> createContainerHeaders = new ArrayList<>();
		
		createContainerHeaders.add(new Header("X-M2M-Origin", /*this.parentApplication.getCredentials()*/"admin:admin"));
		createContainerHeaders.add(new Header("Content-Type", "application/xml;ty=3"));
		createContainerHeaders.add(new Header("X-M2M-NM", this.name));
		
		return createContainerHeaders;
	}
	
	private void sendCreateRemoteContainerRequest(String remoteUrl, String containerXmlNotation, ArrayList<Header> createContainerHeaders) {
		HttpClientImpl containerClient = new HttpClientImpl();
		
		Response createContainerResponse = containerClient.create(remoteUrl, containerXmlNotation, createContainerHeaders);
		if (createContainerResponse.getStatusCode() != 201) {
			System.err.println("Error during creating container, received following response : ");
			System.err.println(createContainerResponse);
		}
	}
	
	public void addRemoteContentInstance(ContentInstance contentInstanceToAdd) {
		contentInstanceToAdd.setParentContainer(this);
		
		contentInstanceToAdd.createRemoteContentInstance();
	}
	
	/*
	 * GETTERS and SETTERS
	 */

	public String getName() {
		return name;
	}
	
	public Application getParentApplication() {
		return parentApplication;
	}

	public void setParentApplication(Application parentApplication) {
		this.parentApplication = parentApplication;
	}
	
	public String getCredentials() {
		// return this.parentApplication.getCredentials();
		return "admin:admin";
	}
}

package om2m.entities;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import obix.Obj;
import obix.io.ObixEncoder;
import om2m.communication.Header;
import om2m.communication.HttpClientImpl;
import om2m.communication.Response;

public class ContentInstance {

	private String contentInfo;
	private Container parentContainer;
	private Obj content;
	
	public static ContentInstance createContentInstance(String contentInfoToAdd, Obj contentToAdd) {
		return new ContentInstance(contentInfoToAdd, contentToAdd);
	}
	
	private ContentInstance(String contentInfoToAdd, Obj contentToAdd) {
		this.contentInfo = contentInfoToAdd;
		this.content = contentToAdd;
	}
	
	private String generateRemoteContainerUrl() {
		String remoteContainerUrl = this.parentContainer.generateRemoteUrl();
		remoteContainerUrl += "/" + this.parentContainer.getName();
		
		return remoteContainerUrl;
	}
	
	private String generateCreateContentInstanceXml() {
		String contentInstanceRepresentation = "<om2m:cin xmlns:om2m=\"http://www.onem2m.org/xml/protocols\">\n";
		contentInstanceRepresentation += "<cnf>" + this.contentInfo + "</cnf>\n";
		contentInstanceRepresentation += "<con>\n" + getFormattedContent() + "</con>\n";
		contentInstanceRepresentation += "</om2m:cin>";
		
		return contentInstanceRepresentation;
	}

	private String getFormattedContent() {
		String formattedContent = ObixEncoder.toString(this.content);
		formattedContent = StringEscapeUtils.escapeHtml4(formattedContent);
		return formattedContent;
	}
	
	private ArrayList<Header> generateCreateContainerHeaders() {
		ArrayList<Header> createContentInstanceHeaders = new ArrayList<>();
		
		createContentInstanceHeaders.add(new Header("X-M2M-Origin", this.parentContainer.getCredentials()));
		createContentInstanceHeaders.add(new Header("Content-Type", "application/xml;ty=4"));
		
		return createContentInstanceHeaders;
	}
	
	public void createRemoteContentInstance() {
		String remoteContainerUrl = generateRemoteContainerUrl();
		String contentInstanceXmlNotation = generateCreateContentInstanceXml();
		ArrayList<Header> createContentInstanceHeaders = generateCreateContainerHeaders();
		
		sendCreateRemoteContentInstanceRequest(remoteContainerUrl, contentInstanceXmlNotation, createContentInstanceHeaders);
	}
	
	private void sendCreateRemoteContentInstanceRequest(String remoteUrl, String contentInstanceXmlNotation, List<Header> createContentInstanceHeaders) {
		HttpClientImpl containerClient = new HttpClientImpl();
		
		Response createContainerResponse = containerClient.create(remoteUrl, contentInstanceXmlNotation, createContentInstanceHeaders);
		if (createContainerResponse.getStatusCode() != 201) {
			System.err.println("Error during creating contentInstance, received following response : ");
			System.err.println(createContainerResponse);
		}
	}
	
	/*
	 * GETTERS and SETTERS
	 */

	public Container getParentContainer() {
		return parentContainer;
	}

	public void setParentContainer(Container parentContainer) {
		this.parentContainer = parentContainer;
	}
}

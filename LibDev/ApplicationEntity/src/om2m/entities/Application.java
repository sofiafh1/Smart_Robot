package om2m.entities;

import java.util.ArrayList;

import om2m.communication.HttpClientImpl;
import om2m.communication.Response;
import om2m.utils.DefaultOM2MParameters;
import om2m.utils.HeaderSet;
import om2m.utils.LabelSet;
import om2m.utils.OM2MParametersInterface;
import om2m.communication.Header;

public class Application {
	
	private String appId;
	private LabelSet labels;
	private HeaderSet applicationHeaderSet;
	private OM2MParametersInterface om2mParameters;
	
	public static Application createApplicationToRemoteOM2M(String appIdToSet, LabelSet labelsToSet, OM2MParametersInterface om2mParameters) {
		return new Application(appIdToSet, labelsToSet, om2mParameters);
	}
	
	/**
	 * Return a default temperature application
	 * 
	 * @param appIdToSet
	 * @return
	 */
	public static Application createDefaultApplicationToRemoteOM2M(String appIdToSet) {
		LabelSet defaultLabels = LabelSet.createDefaultTempSensorLabelSet();
		OM2MParametersInterface defaultOM2MParameters = new DefaultOM2MParameters();
		return new Application(appIdToSet, defaultLabels, defaultOM2MParameters);
	}
	
	private Application(String appIdToSet, LabelSet labelsToSet, OM2MParametersInterface om2mParametersToSet) {
		this.appId = appIdToSet;
		
		this.om2mParameters = om2mParametersToSet;
		
		this.labels = labelsToSet;
		
		createRemoteApplication();
	}

	private void createRemoteApplication() {
		String applicationXmlNotation = generateCreateApplicationXml();
		ArrayList<Header> createApplicationHeaders = generateCreateApplicationHeaders();
		
		sendCreateRemoteApplicationRequest(applicationXmlNotation, createApplicationHeaders);
	}

	private void sendCreateRemoteApplicationRequest(String applicationXmlNotation, ArrayList<Header> createApplicationHeaders) {
		HttpClientImpl applicationClient = new HttpClientImpl();
		
		Response createApplicationResponse = applicationClient.create(this.om2mParameters.getOM2MRootUrl(), applicationXmlNotation, createApplicationHeaders);
		if (createApplicationResponse.getStatusCode() != 201) {
			System.err.println("Error during creating application, received following response : ");
			System.err.println(createApplicationResponse);
		}
	}
	
	private void sendDeleteRemoteApplicationRequest(String remoteDeleteUrl, ArrayList<Header> deleteApplicationHeaders) {
		HttpClientImpl applicationClient = new HttpClientImpl();
		
		Response deleteApplicationResponse = applicationClient.delete(remoteDeleteUrl, deleteApplicationHeaders);
		if (deleteApplicationResponse.getStatusCode() != 200) {
			System.err.println("Error during deleting application, received following response : ");
			System.err.println(deleteApplicationResponse);
		}
	}
	
	private String generateCreateApplicationXml() {
		String xmlRepresentation = "<om2m:ae xmlns:om2m=\"http://www.onem2m.org/xml/protocols\">\n";
		
		xmlRepresentation += "<api>" + this.om2mParameters.getApiType() + "</api>\n";
		
		if (!this.labels.getLabelSet().isEmpty()) {
			// adding searchStrings
			xmlRepresentation += "<lbl>";
			for (String label : this.labels.getLabelSet())
				xmlRepresentation += label + " ";
			xmlRepresentation += "</lbl>\n";
		}
		
		xmlRepresentation += "<rr>false</rr>\n";
		
		// closing representation
		xmlRepresentation += "</om2m:ae>\n";
		
		return xmlRepresentation;
	}
	
	private ArrayList<Header> generateCreateApplicationHeaders() {
		return this.applicationHeaderSet.requestHeaders;
	}
	
	private ArrayList<Header> generateDeleteApplicationHeaders() {
		ArrayList<Header> createApplicationHeaders = new ArrayList<>();

		createApplicationHeaders.add(new Header("X-M2M-Origin", this.om2mParameters.getCredentials()));
		
		return createApplicationHeaders;
	}
	
	private String generateDeleteUrl() {
		return this.om2mParameters.getOM2MRootUrl() + "/mn-name/" + this.appId;
	}
	
	/**
	 * Send a request to the remote server to create a resource for the container under the calling application
	 * 
	 * @param containerToAdd
	 */
	public void addRemoteContainer(Container containerToAdd) {
		containerToAdd.setParentApplication(this);
		
		containerToAdd.createRemoteContainer();
	}
	
	/**
	 * Add itself to the remote server
	 */
	public void addRemoteApplication() {		
		createRemoteApplication();
	}

	/**
	 * Delete the application that is on the remote server
	 */
	public void deleteRemote() {
		String remoteDeleteUrl = generateDeleteUrl();
		ArrayList<Header> deleteApplicationHeaders = generateDeleteApplicationHeaders();
		
		sendDeleteRemoteApplicationRequest(remoteDeleteUrl, deleteApplicationHeaders);
	}
	
	/*
	 * GETTERS and SETTERS
	 */
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
}

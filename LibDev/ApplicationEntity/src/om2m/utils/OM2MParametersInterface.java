package om2m.utils;

/**
 * interface to get parameters such as the OM2MRootUrl, the credentials and the api type
 * 
 * @author gautierenaud
 *
 */
public interface OM2MParametersInterface {

	public abstract String getOM2MRootUrl();
	
	public abstract String getCredentials();
	
	public abstract String getApiType();
}

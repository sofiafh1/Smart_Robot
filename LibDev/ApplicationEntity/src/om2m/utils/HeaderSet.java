package om2m.utils;

import java.util.ArrayList;
import java.util.Iterator;

import om2m.communication.Header;

public class HeaderSet {

	public ArrayList<Header> requestHeaders;
	
	public void changeHeaderValueOrAddNewHeader(String headerKey, String newValueToSet) {
		boolean isHeaderKeyInList = false;
		
		// look for corresponding header from the requestHeaders
		for (Header tmpHeader : requestHeaders) {
			if (tmpHeader.getKey().compareTo(headerKey) == 0) {
				tmpHeader.setValue(newValueToSet);
				isHeaderKeyInList = true;
			}
		}
		
		if (!isHeaderKeyInList)
			addNewHeader(headerKey, newValueToSet);
	}
	
	public void addNewHeader(String headerKey, String newValueToSet) {
		this.requestHeaders.add(new Header(headerKey, newValueToSet));
	}
	
	public void removeHeader(String keyToRemove) {
		for (Iterator<Header> headerIter = this.requestHeaders.listIterator(); headerIter.hasNext(); ) {
		    Header tmpHeader = headerIter.next();
		    if (tmpHeader.getKey().compareTo(keyToRemove) == 0) {
		        headerIter.remove();
		    }
		}
	}
	
	private HeaderSet() {
	}
	
	public static HeaderSet createDefaultRetrieveHeaderSet() {
		HeaderSet headerSetToReturn = new HeaderSet();
		
		headerSetToReturn.addNewHeader("X-M2M-Origin", "admin:admin");
		headerSetToReturn.addNewHeader("Accept", "application/xml");
		
		return headerSetToReturn;
	}
	
	public static HeaderSet createDefaultApplicationHeaderSet(String credentials) {
		HeaderSet headerSetToReturn = new HeaderSet();
		
		headerSetToReturn.addNewHeader("X-M2M-Origin", credentials);
		headerSetToReturn.addNewHeader("Content-Type", "application/xml;ty=2");
		
		return headerSetToReturn;
	}
	
	public static HeaderSet createDefaultContainerHeaderSet() {
		HeaderSet headerSetToReturn = new HeaderSet();
		
		headerSetToReturn.addNewHeader("X-M2M-Origin", "admin:admin");
		headerSetToReturn.addNewHeader("Content-Type", "application/xml;ty=3");
		
		return headerSetToReturn;
	}
	
	public static HeaderSet createDefaultContentInstanceHeaderSet() {
		HeaderSet headerSetToReturn = new HeaderSet();
		
		headerSetToReturn.addNewHeader("X-M2M-Origin", "admin:admin");
		headerSetToReturn.addNewHeader("Content-Type", "application/xml;ty=4");
		
		return headerSetToReturn;
	}
}

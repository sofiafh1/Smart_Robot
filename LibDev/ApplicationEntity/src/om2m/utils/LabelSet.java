package om2m.utils;

import java.util.ArrayList;
import java.util.Iterator;

public class LabelSet {
	private ArrayList<String> labelSet;
	
	// TODO: faire une Hashmap<String, String> pour gérer les labels et faire une fonction d'export en liste de string
	
	public void addLabel(String labelKeyToAdd, String labelValueToAdd) {
		this.labelSet.add(labelKeyToAdd + "/" + labelValueToAdd);
	}
	
	public void changeLabelValueOrAddNewLabel(String labelKey, String labelValueToSet) {
		boolean labelChanged = false;
		String newWholeLabel = labelKey + "/" + labelValueToSet;
		
		for (String tmpLabel : this.labelSet) {
			String tmpLabelKey = tmpLabel.split("/")[0];
			if (tmpLabelKey.compareTo(labelKey) == 0) {
				tmpLabel = newWholeLabel;
				labelChanged = true;
			}
		}
		
		if (!labelChanged) {
			addLabel(labelKey, labelValueToSet);
		}
	}
	
	public void removeLabelFromKey(String labelKeyToRemove) {
		for (Iterator<String> labelIter = this.labelSet.listIterator(); labelIter.hasNext(); ) {
		    String tmpLabel = labelIter.next();
		    String tmpLabelKey = tmpLabel.split("/")[0];
		    if (tmpLabelKey.compareTo(labelKeyToRemove) == 0) {
		        labelIter.remove();
		    }
		}
	}

	public ArrayList<String> getLabelSet() {
		return this.labelSet;
	}
	
	private LabelSet() {
		labelSet = new ArrayList<>();
	}
	
	public static LabelSet createEmptyLabelSet() {
		LabelSet labelSetToReturn = new LabelSet();
		return labelSetToReturn;
	}
	
	public static LabelSet createDefaultTempSensorLabelSet() {
		LabelSet labelSetToReturn = new LabelSet();
		
		labelSetToReturn.addLabel("Type", "sensor");
		labelSetToReturn.addLabel("Category", "temperature");
		labelSetToReturn.addLabel("Location", "home");
		
		return labelSetToReturn;
	}
	
	public static LabelSet createDefaultProximitySensorLabelSet() {
		LabelSet labelSetToReturn = new LabelSet();
		
		labelSetToReturn.addLabel("Type", "sensor");
		labelSetToReturn.addLabel("Category", "presence");
		labelSetToReturn.addLabel("Location", "home");
		
		return labelSetToReturn;
	}
}

package om2m.utils;

public class DefaultOM2MParameters implements OM2MParametersInterface {
	
	@Override
	public String getOM2MRootUrl() {
		return "http://localhost:8080/~/mn-cse";
	}

	@Override
	public String getCredentials() {
		return "admin:admin";
	}

	@Override
	public String getApiType() {
		return "api-sensor";
	}
}
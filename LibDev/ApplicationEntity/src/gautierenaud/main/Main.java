package gautierenaud.main;

import fr.v2.om2m.entities.ApplicationEntity;
import fr.v2.om2m.entities.Container;
import fr.v2.om2m.entities.ContentInstance;
import fr.v2.om2m.utils.ApplicationEntityConfig;
import fr.v2.om2m.utils.ContainerConfig;
import fr.v2.om2m.utils.ContentInstanceConfig;
import fr.v2.om2m.utils.HeaderSet;
import obix.Str;

public class Main {

	public static void main(String[] args) {
		// test the new version
		ApplicationEntityConfig aeConfigTest = new ApplicationEntityConfig();
		aeConfigTest.labels.setLabel("Type", "Sensor");
		aeConfigTest.labels.setLabel("Location", "Home");
		aeConfigTest.headers = HeaderSet.generateDefaultApplicationEntityHeaderSet();
		
		ApplicationEntity aeTest = ApplicationEntity.createApplicationOnRemote(aeConfigTest);
		
		ContainerConfig contConfigTest = new ContainerConfig();
		contConfigTest.headers = HeaderSet.generateDefaultContainerHeaderSet();
		contConfigTest.resourceName = "DESCRIPTOR";
		contConfigTest.parentApplication = aeTest;
		
		Container contTest = Container.createContainerOnRemote(contConfigTest);
		
		ContentInstanceConfig contInstConfTest = new ContentInstanceConfig();
		contInstConfTest.content.add(new Str("type", "Temperature_Sensor"));
		contInstConfTest.content.add(new Str("location", "Home"));
		contInstConfTest.content.add(new Str("appId", "Toto"));
		contInstConfTest.parentContainer = contTest;
		contInstConfTest.headers = HeaderSet.generateDefaultContentInstanceHeaderSet();
		
		ContentInstance.createContentInstanceOnRemote(contInstConfTest);
		
		// contTest.removeContainerFromRemote();
		// aeTest.deleteApplicationOnRemote();
		
		
		/*
		String appId = "MY_SENSOR";
		LabelSet labels = LabelSet.createDefaultTempSensorLabelSet();
		OM2MParametersInterface om2mParams = new DefaultOM2MParameters();
		
		Application sensorApplication = Application.createApplicationToRemoteOM2M(appId, labels, om2mParams);
		sensorApplication.addRemoteApplication();
		
		String containerName = "DESCRIPTOR";
		Container descriptorContainer = Container.createContainer(containerName);
		
		sensorApplication.addRemoteContainer(descriptorContainer);
		
		String contentInfo = "message";
		Obj descriptionInstance = new Obj();
		descriptionInstance.add(new Str("type", "Temperature_Sensor"));
		descriptionInstance.add(new Str("location", "Home"));
		descriptionInstance.add(new Str("appId", appId));
		ContentInstance dataInstance = ContentInstance.createContentInstance(contentInfo, descriptionInstance);
		
		descriptorContainer.addRemoteContentInstance(dataInstance);
		
		sensorApplication.deleteRemote();
		*/
	}

}

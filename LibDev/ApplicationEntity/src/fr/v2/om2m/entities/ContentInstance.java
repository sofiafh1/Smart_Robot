package fr.v2.om2m.entities;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import fr.v2.om2m.utils.ContentInstanceConfig;
import obix.io.ObixEncoder;
import om2m.communication.Header;
import om2m.communication.HttpClientImpl;
import om2m.communication.Response;

public class ContentInstance {
	
	private ContentInstanceConfig contInstConfig;
	
	public static ContentInstance createContentInstanceOnRemote(ContentInstanceConfig contInstConfig) {
		return new ContentInstance(contInstConfig);
	}
	
	private ContentInstance(ContentInstanceConfig contInstConfig) {
		this.contInstConfig = contInstConfig;
		
		checkContInstConfig();
		
		createRemoteContentInstance();
	}
	
	private void checkContInstConfig() {
		if (this.contInstConfig.parentContainer == null) {
			System.err.println("parentContainer not setted, the contentInstance will not be able to get the target url");
		}
	}
	
	private void createRemoteContentInstance() {
		String contentInstanceXmlNotation = generateCreateContentInstanceXml();
		ArrayList<Header> createContentInstanceHeaders = generateCreateContainerHeaders();
		
		sendCreateRemoteContentInstanceRequest(contentInstanceXmlNotation, createContentInstanceHeaders);
	}
	
	private String generateCreateContentInstanceXml() {
		String contentInstanceRepresentation = "<om2m:cin xmlns:om2m=\"http://www.onem2m.org/xml/protocols\">\n";
		contentInstanceRepresentation += "<cnf>" + this.contInstConfig.contentInfo + "</cnf>\n";
		contentInstanceRepresentation += "<con>\n" + getHTMLFormattedContent() + "</con>\n";
		contentInstanceRepresentation += "</om2m:cin>";
		
		return contentInstanceRepresentation;
	}

	private String getHTMLFormattedContent() {
		String formattedContent = ObixEncoder.toString(this.contInstConfig.content);
		formattedContent = StringEscapeUtils.escapeHtml4(formattedContent);
		return formattedContent;
	}
	
	private ArrayList<Header> generateCreateContainerHeaders() {
		ArrayList<Header> createContentInstanceHeaders = (ArrayList<Header>) this.contInstConfig.headers.getHeaderList();
		
		if (!this.contInstConfig.headers.containHeader("X-M2M-Origin")) {
			createContentInstanceHeaders.add(new Header("X-M2M-Origin", this.contInstConfig.parentContainer.getContainerCredentials()));
		}
		
		return createContentInstanceHeaders;
	}
	
	private void sendCreateRemoteContentInstanceRequest(String contentInstanceXmlNotation, List<Header> createContentInstanceHeaders) {
		HttpClientImpl containerClient = new HttpClientImpl();
		String remoteContainerUrl = this.contInstConfig.parentContainer.getContainerUrl();
		
		Response createContainerResponse = containerClient.create(remoteContainerUrl, contentInstanceXmlNotation, createContentInstanceHeaders);
		if (createContainerResponse.getStatusCode() != 201) {
			System.err.println("Error during creating contentInstance, received following response : ");
			System.err.println(createContainerResponse);
		}
	}
}

package fr.v2.om2m.entities;

import java.util.ArrayList;

import fr.v2.om2m.utils.ApplicationEntityConfig;
import om2m.communication.Header;
import om2m.communication.HttpClientImpl;
import om2m.communication.Response;

public class ApplicationEntity {

	private ApplicationEntityConfig aeConfig;
	
	private ApplicationEntity(ApplicationEntityConfig aeConfigToSet) {
		this.aeConfig = aeConfigToSet;
		
		createRemoteApplication();
	}
	
	public static ApplicationEntity createApplicationOnRemote(ApplicationEntityConfig aeConfigToSet) {
		ApplicationEntity aeToReturn = new ApplicationEntity(aeConfigToSet);
		
		return aeToReturn;
	}
	
	public void removeApplicationFromRemote() {
		ArrayList<Header> deleteApplicationHeaders = generateRemoveApplicationHeaders();
		
		sendRemoveRemoteApplicationRequest(deleteApplicationHeaders);
	}
	
	private void createRemoteApplication() {
		String applicationXmlNotation = generateCreateApplicationXml();
		ArrayList<Header> createApplicationHeaders = generateCreateApplicationHeaders();
		
		sendCreateRemoteApplicationRequest(applicationXmlNotation, createApplicationHeaders);
	}
	
	private String generateCreateApplicationXml() {
		String xmlRepresentation = "<om2m:ae xmlns:om2m=\"http://www.onem2m.org/xml/protocols\" rn=\"" + this.aeConfig.resourceName + "\">\n";
		
		xmlRepresentation += "<api>" + this.aeConfig.apiType + "</api>\n";
		
		if (!this.aeConfig.labels.isEmpty()) {
			xmlRepresentation += "<lbl>" + this.aeConfig.labels.getFormattedLabelsValues() + "</lbl>\n";
		}
		
		xmlRepresentation += "<rr>" + this.aeConfig.requestReachability + "</rr>\n";
		
		if (this.aeConfig.requestReachability) {
			xmlRepresentation += "<poa>" + this.aeConfig.poas.getFormattedPoas() + "</poa>";
		}
		
		// closing representation
		xmlRepresentation += "</om2m:ae>\n";
		
		return xmlRepresentation;
	}
	
	private ArrayList<Header> generateCreateApplicationHeaders() {
		ArrayList<Header> createAppHeaders = (ArrayList<Header>) this.aeConfig.headers.getHeaderList();
		
		if (!this.aeConfig.headers.containHeader("X-M2M-Origin")) {
			createAppHeaders.add(new Header("X-M2M-Origin", this.aeConfig.credentials));
		}
		
		return createAppHeaders;
	}
	
	private ArrayList<Header> generateRemoveApplicationHeaders() {
		ArrayList<Header> removeAppHeaders = new ArrayList<>();
		removeAppHeaders.add(new Header("X-M2M-Origin", this.aeConfig.credentials));
		return removeAppHeaders;
	}
	
	private void sendCreateRemoteApplicationRequest(String aeXmlNotationToSend, ArrayList<Header> aeHeadersToSet) {
		HttpClientImpl applicationClient = new HttpClientImpl();
		
		Response createApplicationResponse = applicationClient.create(this.aeConfig.rootUrl, aeXmlNotationToSend, aeHeadersToSet);
		if (createApplicationResponse.getStatusCode() != 201) {
			System.err.println("Error during creating application, received following response : ");
			System.err.println(createApplicationResponse);
		} else {
			System.out.println("Application created on remote");
		}
	}
	
	private void sendRemoveRemoteApplicationRequest(ArrayList<Header> aeHeadersToSet) {
		HttpClientImpl applicationClient = new HttpClientImpl();
		String remoteAppUrl = this.aeConfig.rootUrl + "/mn-name/" + this.aeConfig.resourceName;
		
		Response removeApplicationResponse = applicationClient.delete(remoteAppUrl, aeHeadersToSet);
		if (removeApplicationResponse.getStatusCode() != 200) {
			System.err.println("Error during removing application, received following response : ");
			System.err.println(removeApplicationResponse);
		} else {
			System.out.println("Application removing on remote");
		}
	}
	
	public String getCredentials() {
		return this.aeConfig.credentials;
	}
	
	public String getApplicationUrl() {
		return this.aeConfig.rootUrl + "/mn-name/" + this.aeConfig.resourceName;
	}
}
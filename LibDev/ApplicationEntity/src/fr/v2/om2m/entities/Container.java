package fr.v2.om2m.entities;

import java.util.ArrayList;

import fr.v2.om2m.utils.ContainerConfig;
import om2m.communication.Header;
import om2m.communication.HttpClientImpl;
import om2m.communication.Response;

public class Container {

	private ContainerConfig contConfig;
	
	private Container(ContainerConfig contConfig) {
		this.contConfig = contConfig;
		
		createRemoteContainer();
	}
	
	public static Container createContainerOnRemote(ContainerConfig contConfig) {
		Container contToCreate = new Container(contConfig);
		return contToCreate;
	}
	
	public void removeContainerFromRemote() {
		ArrayList<Header> deleteContainerHeaders = generateRemoveContainerHeaders();
		
		sendRemoveRemoteContainerRequest(deleteContainerHeaders);
	}
	
	private void createRemoteContainer() {
		String containerXmlNotation = generateCreateContainerXml();
		ArrayList<Header> createContainerHeaders = generateCreateContainerHeaders();
		
		sendCreateRemoteContainerRequest(containerXmlNotation, createContainerHeaders);
	}
	
	private String generateCreateContainerXml() {
		String containerRepresentation = "<om2m:cnt xmlns:om2m=\"http://www.onem2m.org/xml/protocols\" rn=\"" + this.contConfig.resourceName + "\"></om2m:cnt>";
		return containerRepresentation;
	}
	
	private ArrayList<Header> generateCreateContainerHeaders() {
		ArrayList<Header> createContHeaders = (ArrayList<Header>) this.contConfig.headers.getHeaderList();
		
		if (!this.contConfig.headers.containHeader("X-M2M-Origin")) {
			createContHeaders.add(new Header("X-M2M-Origin", getParentApplicationCredentials()));
		}
		
		return createContHeaders;
	}
	
	private ArrayList<Header> generateRemoveContainerHeaders() {
		ArrayList<Header> removeContHeaders = new ArrayList<>();
		removeContHeaders.add(new Header("X-M2M-Origin", getParentApplicationCredentials()));
		return removeContHeaders;
	}
	
	private void sendCreateRemoteContainerRequest(String contXmlNotationToSend, ArrayList<Header> contHeadersToSet) {
		HttpClientImpl containerClient = new HttpClientImpl();
		String containerRootUrl = getParentApplicationRootUrl();
		
		Response createContainerResponse = containerClient.create(containerRootUrl, contXmlNotationToSend, contHeadersToSet);
		if (createContainerResponse.getStatusCode() != 201) {
			System.err.println("Error during creating container, received following response : ");
			System.err.println(createContainerResponse);
		} else {
			System.out.println("Container created on remote");
		}
	}
	
	private void sendRemoveRemoteContainerRequest(ArrayList<Header> contHeadersToSet) {
		HttpClientImpl containerClient = new HttpClientImpl();
		String containerRootUrl = getParentApplicationRootUrl() + "/" + this.contConfig.resourceName;
		
		Response removeContainerResponse = containerClient.delete(containerRootUrl, contHeadersToSet);
		if (removeContainerResponse.getStatusCode() != 200) {
			System.err.println("Error during removing container, received following response : ");
			System.err.println(removeContainerResponse);
		} else {
			System.out.println("Container removed on remote");
		}
	}
	
	private String getParentApplicationCredentials() {
		return this.contConfig.parentApplication.getCredentials();
	}
	
	private String getParentApplicationRootUrl() {
		return this.contConfig.parentApplication.getApplicationUrl();
	}
	
	public String getContainerUrl() {
		return getParentApplicationRootUrl() + "/" + this.contConfig.resourceName;
	}
	
	public String getContainerCredentials() {
		return getParentApplicationCredentials();
	}
}

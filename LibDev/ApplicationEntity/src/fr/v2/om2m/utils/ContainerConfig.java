package fr.v2.om2m.utils;

import fr.v2.om2m.entities.ApplicationEntity;

public class ContainerConfig {
	public String resourceName = "MY_CONTAINER";
	public HeaderSet headers = new HeaderSet();
	public ApplicationEntity parentApplication;
}

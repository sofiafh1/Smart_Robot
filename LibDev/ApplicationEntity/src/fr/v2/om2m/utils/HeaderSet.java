package fr.v2.om2m.utils;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import om2m.communication.Header;

public class HeaderSet {

	private Hashtable<String, String> headerTable;
	
	public HeaderSet() {
		this.headerTable = new Hashtable<>();
	}
	
	public static HeaderSet generateDefaultApplicationEntityHeaderSet() {
		HeaderSet defaultContainerHeaderSet = new HeaderSet();
		defaultContainerHeaderSet.setHeader("Content-Type", "application/xml;ty=2");
		return defaultContainerHeaderSet;
	}
	
	public static HeaderSet generateDefaultContainerHeaderSet() {
		HeaderSet defaultContainerHeaderSet = new HeaderSet();
		defaultContainerHeaderSet.setHeader("Content-Type", "application/xml;ty=3");
		return defaultContainerHeaderSet;
	}
	
	public static HeaderSet generateDefaultContentInstanceHeaderSet() {
		HeaderSet defaultContentInstanceHeaderSet = new HeaderSet();
		defaultContentInstanceHeaderSet.setHeader("Content-Type", "application/xml;ty=4");
		return defaultContentInstanceHeaderSet;
	}
	
	public void setHeader(String headerKeyToAdd, String headerValueToAdd) {
		this.headerTable.put(headerKeyToAdd, headerValueToAdd);
	}
	
	public void setHeader(Header headerToAdd) {
		this.headerTable.put(headerToAdd.getKey(), headerToAdd.getValue());
	}
	
	public void removeHeader(String headerKeyToDelete) {
		this.headerTable.remove(headerKeyToDelete);
	}
	
	public String getHeaderValue(String headerKeyToGet) {
		return this.headerTable.get(headerKeyToGet);
	}
	
	public Header getHeader(String headerKeyToGet) {
		String headerValueToGet = this.headerTable.get(headerKeyToGet);
		return new Header(headerKeyToGet, headerValueToGet);
	}
	
	public boolean containHeader(String headerKey) {
		return this.headerTable.containsKey(headerKey);
	}
	
	public List<Header> getHeaderList() {
		ArrayList<Header> headerListToReturn = new ArrayList<>();
		for (String headerKey : this.headerTable.keySet()) {
			Header tmpHeader = getHeader(headerKey);
			headerListToReturn.add(tmpHeader);
		}
		return headerListToReturn;
	}
}

package fr.v2.om2m.utils;

import fr.v2.om2m.entities.Container;
import obix.Obj;

public class ContentInstanceConfig {
	public String contentInfo = "default-message";
	public Obj content = new Obj();
	public HeaderSet headers = new HeaderSet();
	public Container parentContainer = null;
}

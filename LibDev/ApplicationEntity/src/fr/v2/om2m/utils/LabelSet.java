package fr.v2.om2m.utils;

import java.util.Hashtable;

public class LabelSet {

	private Hashtable<String, String> labelTable;
	
	public LabelSet() {
		this.labelTable = new Hashtable<>();
	}
	
	public void setLabel(String labelKeyToAdd, String labelValueToAdd) {
		this.labelTable.put(labelKeyToAdd, labelValueToAdd);
	}
	
	public void removeLabel(String labelKeyToRemove) {
		this.labelTable.remove(labelKeyToRemove);
	}
	
	public String getLabelValue(String labelKeyToGet) {
		String labelValueToReturn = null;
		if (this.labelTable.containsKey(labelKeyToGet)) {
			labelValueToReturn = this.labelTable.get(labelKeyToGet);
		}
		
		return labelValueToReturn;
	}
	
	/**
	 * return the label in "key/value" format
	 * 
	 * @param labelKeyToFormat
	 * @return
	 */
	public String getFormattedLabelValue(String labelKeyToFormat) {
		String labelValueToFormat = getLabelValue(labelKeyToFormat);
		
		String formattedLabel = labelKeyToFormat + "/" + labelValueToFormat;
		
		return formattedLabel;
	}
	
	public String getFormattedLabelsValues() {
		String formattedString = "";
		
		for (String labelKey : this.labelTable.keySet()) {
			formattedString += getFormattedLabelValue(labelKey) + " ";
		}
		
		return formattedString;
	}
	
	public boolean isEmpty() {
		return this.labelTable.isEmpty();
	}
}

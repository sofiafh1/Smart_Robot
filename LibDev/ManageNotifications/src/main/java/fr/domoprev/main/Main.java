package fr.domoprev.main;

import java.util.ArrayList;
import java.util.Scanner;

import fr.domoprev.om2m.core.Discovery;
import fr.domoprev.om2m.core.Monitor;
import fr.domoprev.om2m.core.Subscription;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Type the base cse name : \n");
		String baseCseName = scan.nextLine();
		if(baseCseName.equals("mn-cse")){
			System.out.println("Type the label name : \n");
			String label = scan.nextLine();
			Thread monitor = new Thread(new Monitor());
			Thread discovery = new Thread(new Discovery(baseCseName,label));
			//Thread discovery = new Thread(new Discovery("mn-cse", "Type/sensor"));
			Subscription subscribe = new Subscription() ;
			try {
				monitor.start();
				discovery.start();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			while (true){
	            String inputUser = scan.nextLine().toLowerCase();
	            if(inputUser.equalsIgnoreCase("s")){
	                System.out.println("stopping program .... ");   
	            	if(subscribe.isDeletedSAllSubscription()){
	                	System.exit(0);
	            	}
	            }
	        }
		}
		else {
			System.err.println("Error bad Base Cse Name");
		}
		
	}

}

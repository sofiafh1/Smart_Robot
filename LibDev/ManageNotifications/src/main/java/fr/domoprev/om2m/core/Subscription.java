package fr.domoprev.om2m.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.domoprev.om2m.client.Header;
import fr.domoprev.om2m.client.HttpClientImpl;
import fr.domoprev.om2m.client.Response;

public class Subscription {
	private String http;
	private String credentials;
	private String subsribeInfo ;
	private static HashMap<String, String> subscribedList = new HashMap<String, String>();
	private static HashMap<String, String> notInSubscribedList = new HashMap<String, String>();
	
	public Subscription() {
		this.http = "http://127.0.0.1:8080";
		this.credentials = "admin:admin" ;
		this.subsribeInfo = "http://localhost:1400/monitor";
	}
	
	public void sendSubscribeRequests(ArrayList<String> appList){
		Map<String, String> subscribedRequestList = new HashMap<String, String>();
		subscribedRequestList = isNotSubscribed(appList);
		System.out.println("size :" +subscribedRequestList.size());
		if(!subscribedRequestList.isEmpty()){
			for(Map.Entry<String, String> subscribeUrl : subscribedRequestList.entrySet()){
				ArrayList<Header> subscribeHeaders = generateSubscribeHeaders();
				String bodyXml = generateSubscribeBody(subscribeUrl.getValue());
				HttpClientImpl  httpClientImpl = new HttpClientImpl();
				Response subscribeResponse = httpClientImpl.create(subscribeUrl.getKey(), bodyXml, subscribeHeaders);
				if (subscribeResponse.getStatusCode() != 201) {
					System.err.println("Error during subscription, received following response : ");
					System.err.println(subscribeResponse);
				}
				else{
					notInSubscribedList.clear();
					System.out.println("*******Finish subscription*******");
				}
				
			}
			
		}
		else {
			System.out.println("*******All subscription has been done *******");
		}
		
	}
	
	private String generateSubscribeUrl(String uril){
		String subscibeUrl = http +"/~" +uril+"/DATA";
		System.out.println("*******subscribeUrl : ******* \n" +subscibeUrl);
		
		return subscibeUrl;
	}
	
	private ArrayList<Header> generateSubscribeHeaders() {
		ArrayList<Header> subscribeHeaders = new ArrayList<Header>();
		subscribeHeaders.add(new Header("X-M2M-Origin", this.credentials));
		subscribeHeaders.add(new Header("Content-Type", "application/xml;ty=23"));
		subscribeHeaders.add(new Header("Accept", "application/xml"));
		
		return subscribeHeaders;
	}
	
	private String generateSubscribeBody(String appName){
		String bodyXml = "<m2m:sub xmlns:m2m=\"http://www.onem2m.org/xml/protocols\" rn=\""+appName+"\">\n";
		bodyXml += "<nu>" + this.subsribeInfo + "</nu>\n";
		bodyXml += "<nct>" + 2 + "</nct>\n";
		bodyXml += "</m2m:sub>";
		
		return bodyXml ;
	}
	
	private String getAppNameFromUril(String uril){
		String[] content = uril.split("/");
		String appName = content[3];
		return appName;
		
	}
	
	private HashMap<String,String> isNotSubscribed(ArrayList<String> uril){
		String subscriptionUrlKey;
		String subscriptionUrlValue;
		for(String element : uril){
			subscriptionUrlKey = generateSubscribeUrl(element);
			subscriptionUrlValue = "SUB_" + getAppNameFromUril(element);
			if(!subscribedList.containsKey(subscriptionUrlKey)){
				subscribedList.put(subscriptionUrlKey, subscriptionUrlValue);
				notInSubscribedList.put(subscriptionUrlKey, subscriptionUrlValue);
			}
			
		}
		
		return notInSubscribedList ;
	}
	public Boolean isDeletedSAllSubscription(){
		String urlSubscriptionToDelete;
		String subscriptionName ;
		Boolean result = null ;
		for(Map.Entry<String, String> element : subscribedList.entrySet() ){
			urlSubscriptionToDelete = element.getKey();
			subscriptionName = element.getValue();
			ArrayList<Header> deleteHeaders = generateSubscribeHeaders();
			HttpClientImpl  httpClientImpl = new HttpClientImpl();
			Response deleteResponse = httpClientImpl.delete(urlSubscriptionToDelete +"/"+ subscriptionName, deleteHeaders);
			if(deleteResponse.getStatusCode() !=200){
				System.err.println("Error during delete subscription, received following response : ");
				System.err.println(deleteResponse);
				result = false;
			}
			else {
				subscribedList.clear();
				System.out.println("*******Finish all subscription has been delete*******");
				result = true ;
			}
		}
		return result ;
	}
}

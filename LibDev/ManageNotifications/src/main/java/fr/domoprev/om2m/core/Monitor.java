package fr.domoprev.om2m.core;


import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.domoprev.om2m.mapper.Mapper;
import fr.domoprev.om2m.util.NotificationUtil;
import fr.domoprev.om2m.util.NotificationUtil.NotObixContentException;
import obix.Int;
import obix.Obj;
import obix.Str;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.om2m.commons.resource.Notification;


public class Monitor implements Runnable {

	private static Mapper mapper = new Mapper();
	private static Obj objFromNotification = null ;
	private static Map<String, Object> valueFromNotification = new HashMap<String, Object>();
	private static int PORT = 1400;
	private static String CONTEXT = "/monitor";
	
	public Monitor() {
	}
	/**
	 * Get the payload as string
	 * 
	 * @param bufferedReader
	 * @return payload as string
	 */
	public static String getPayload(BufferedReader bufferedReader) {
		Scanner sc = new Scanner(bufferedReader);
		String payload = "";
		while (sc.hasNextLine()) {
			payload += sc.nextLine() + "\n";
		}
		sc.close();
		return payload;
	}

	public static class MonitorServlet extends HttpServlet {

		private static final long serialVersionUID = 2302036096330714914L;

		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			String payload = getPayload(req.getReader());
			System.out.println("Subscription received with payload:\n"
					+ payload);

			/*
			 *  unmarshalling the notification
			 *  extracts data contains in the notification
			 */
			Notification notification = null;
			notification = (Notification)mapper.unmarshal(payload);
			if(notification.isVerificationRequest()==null){
				try {
					objFromNotification = NotificationUtil.getObixFromNotification(notification);
					Obj data = objFromNotification.get("data");
					Obj category = objFromNotification.get("category");
					if(category instanceof Str){
						valueFromNotification.put("category", (Str)category);
					}
					if(data instanceof Int){
						valueFromNotification.put("data", (Int)data);
					}
					
				} catch (NotObixContentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(valueFromNotification.containsKey("category") && valueFromNotification.containsKey("data")){
				String sensorCategory = valueFromNotification.get("category").toString();
				int dataValue = Integer.parseInt(valueFromNotification.get("data").toString());
				System.out.println("********Notification data *********\n");
				System.out.println("Sensor : " +sensorCategory + " " +"dataValue :" + dataValue);
				}

			resp.setStatus(HttpServletResponse.SC_OK);

		}

	}

	public void run() {
		// start the server
		Server server = new Server(PORT);
		System.out.println("new Server(PORT)");
		ServletHandler servletHandler = new ServletHandler();
		
		// add servlet and context
		servletHandler.addServletWithMapping(MonitorServlet.class, CONTEXT);
		System.out.println("servletHandler.addServletWithMapping");
		server.setHandler(servletHandler);
		System.out.println("server.setHandler");
		try {
			server.start();
			System.out.println("server.start()");
			server.join();
			System.out.println("server.join()");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

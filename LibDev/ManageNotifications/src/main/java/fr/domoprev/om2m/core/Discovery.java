package fr.domoprev.om2m.core;

import java.util.ArrayList;

import fr.domoprev.om2m.client.Header;
import fr.domoprev.om2m.client.HttpClientImpl;
import fr.domoprev.om2m.client.Response;
import gvjava.org.json.JSONException;
import gvjava.org.json.JSONObject;

public class Discovery implements Runnable {
	
	private String baseCseName;
	private String label;
	private String credentials;
	private String http;
	
	/**
	 *  Initialize the baseCseName and label provided
	 *  @param baseCseName, label
	 */
	public Discovery(String baseCseName, String label) {
		this.baseCseName = baseCseName;
		this.label = label;
		this.credentials = "admin:admin" ;
		this.http = "http://127.0.0.1:8080";
	}
	/**
	 * Make discovery request to get all applicationEntity from Om2m
	 * @return response as ArrayList<String> 
	 */
	public ArrayList<String> sendDiscoveryRequest(){
		ArrayList<Header> discoveryHeaders = generateDiscoveryHeaders();
		ArrayList<String> appList = new ArrayList<String>() ;
		String discoveryUrl = generateDiscoveryUrl();
		HttpClientImpl  httpClientImpl = new HttpClientImpl();
		Response discoveryResponse = httpClientImpl.retrieve(discoveryUrl, discoveryHeaders);
		if (discoveryResponse.getStatusCode() != 200) {
			System.err.println("Error during discovery, received following response : ");
			System.err.println(discoveryResponse);
		}
		else {
			appList = extractUrilFromJson(discoveryResponse.getRepresentation());
		}
		
		return appList;
	}
	/**
	 * Generate Discovery url 
	 * @return url as string
	 */
	private String generateDiscoveryUrl(){
		String discoveryUrl = http +"/~/" +baseCseName +"?fu=1&" + "lbl=" + label;
		return discoveryUrl;
	}
	/**
	 * Generate Discovery headers 
	 * @return response as ArrayList<String> of headers
	 */
	private ArrayList<Header> generateDiscoveryHeaders() {
		ArrayList<Header> discoveryHeaders = new ArrayList<Header>();
		discoveryHeaders.add(new Header("X-M2M-Origin", this.credentials));
		discoveryHeaders.add(new Header("Accept", "application/json"));
		
		return discoveryHeaders;
	}
	
	/**
	 * Extract uril from the discovery response 
	 * @param String in json format
	 * @return List of uril 
	 */
	public ArrayList<String> extractUrilFromJson(String jsonInString){
		ArrayList<String> appList = new ArrayList<String>() ;
		try {
			JSONObject jsonObject = new JSONObject(jsonInString.trim());
			String discoveryUril = jsonObject.getString("m2m:uril");
			for (String element : discoveryUril.split(" ")){
				appList.add(element);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return appList ;
	}
	public void run(){
		while(true){
			ArrayList<String> appList = new ArrayList<String>() ;
			appList = sendDiscoveryRequest();	
			Subscription subscription = new Subscription();
			subscription.sendSubscribeRequests(appList);
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}
